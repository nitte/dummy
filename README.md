Open source software to collaborate on code
To see how GitLab looks please see the features page on our website.
Manage Git repositories with fine grained access controls that keep your code secure
Perform code reviews and enhance collaboration with merge requests
Each project can also have an issue tracker and a wiki
Used by more than 100,000 organizations, GitLab is the most popular solution to manage Git repositories on-premises
Completely free and open source (MIT Expat license)
Powered by Ruby on Rails