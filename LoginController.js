//---------------------METHOD WHICH WILL BECOME SERVICE-------------------------

//-----------------------URI-----------------------

var URI = getURI();

// -----------------------REGISTERING THE APPLICATION AND
// DEPENDENCIES-----------------------

var infyRetail = angular.module("InfyRetails", [ 'ngCookies' ]);

// ----------------------REGISTERING THE SERVICE WITH THE
// APPLICATION------------------------

// -------------------------REGISTERING THE FILTERS WITH THE
// APPLICATION---------------------

// ----------------------CONFIGURING THE APPLICATION------------------------

infyRetail
		.config(function($httpProvider) {
			$httpProvider.defaults.headers.post['Content-Type'] = "application/json; charset=UTF-8";
			$httpProvider.defaults.headers.post['Data-Type'] = "json";
		});

// -----------------------CONTROLLERS-----------------------
infyRetail
		.controller(
				"LoginController",
				function($scope, $http, $cookies) {
					$scope.loginForm = {};
					$scope.loginForm.message = null;
					$scope.loginForm.checkUserDetails = function() {
						var data = angular.toJson($scope.loginForm);
						var responsePromise = $http
								.post(URI + "LoginAPI", data);
						responsePromise
								.then(
										function(response) {
											$cookies.put("name",
													response.data.userName);
											$cookies.put("Id",
													response.data.userId);
											$cookies.put("role",
													response.data.userRole);
											if (response.data.userRole == 'CUSTOMER'
													|| response.data.userRole == 'SUPPLIER')
												window.location = "partials/customer_retailer/CustomerRetailerHome.html";
											else
												window.location = "partials/admin/AdminHome.html";
										},
										function(response) {
											$scope.loginForm.message = response.data.message;
										});
					}
				});
